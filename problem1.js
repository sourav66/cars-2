
/* ==== Problem #1 ====
The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
*/


function search(inventory, num)
{
    let ans = [];
    if (typeof inventory === 'undefined')
        return ans;
    else if(inventory.length <= 0) //Added a checking for an empty inventory.
        return ans;
    else if (typeof num === 'undefined')
        return ans;
    else if(Number.isInteger(num) === false)
        return ans;
        
    else if (Array.isArray(inventory) === false)
    {
        return ans;
    }
    else
    {
        for(let i = 1; i<=inventory.length; i++)
        {
            if(num === inventory[i-1].id) //Added a checking for invalid search ID being passed to the function
            {
                ans = inventory[i-1];
                return ans;
            }
        }
    }
    
    return ans;
}
module.exports = search;