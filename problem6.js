/*==== Problem #6 ====
A buyer is interested in seeing only BMW and Audi cars within the inventory.
Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
*/


let inventory = require("./index.js");
function carMake(car)
{
    let arr = [], p = 0;
    if(inventory.length <= 0) //Added a checking for an empty inventory.
        return arr;
    for(let i=0;i < inventory.length;i ++)
    {
         let obj = inventory[i];
         let s = obj.car_make
         
         if(s === car)
            {
                arr[p] = inventory[i];
                p++;
            }
    }
    return (arr);
}
module.exports = carMake;
carMake();