/*==== Problem #4 ====
The accounting team needs all the years from every car on the lot.
Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
*/

let inventory = require("./index.js");
function yearList()
{
    if(inventory.length <= 0) //Added a checking for an empty inventory.
        return inventory;
        
    var arr = [];
    for(let i=0;i < inventory.length ;i ++)
    {
         let obj = inventory[i];
         arr[i] = obj.car_year;
    }
    return (arr);
    
}
module.exports = yearList;
yearList();